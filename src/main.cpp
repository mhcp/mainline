#include <QtDebug>
#include <QApplication>

#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>

#include "mhcpapp.h"

using namespace mhcp;

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	app.setQuitOnLastWindowClosed(false);

	MhcpApp mhcp(&app);

	return app.exec();
}
