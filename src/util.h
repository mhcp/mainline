#ifndef __UTIL_H__
#define __UTIL_H__

#include <QTime>

namespace mhcp {

	QTime qtimeFromMSecs(int msecs);
	QTime substractTime(QTime t1, QTime t2);
	QTime addTime(QTime t1, QTime t2);


} // namespace mhcp

#endif
