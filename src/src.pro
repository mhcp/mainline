TEMPLATE = app

QT += sql

CONFIG+= console

TARGET = ../bin/mhcp

OBJECTS_DIR = tmp

UI_DIR = tmp

MOC_DIR = tmp

FORMS += configdialog.ui \
historydialog.ui \
aboutdialog.ui

SOURCES += hcpconfig.cpp \
historydialog.cpp \
main.cpp \
mhcpapp.cpp \
util.cpp \
aboutdialog.cpp \
editablelabel.cpp
HEADERS += hcpconfig.h \
historydialog.h \
mhcpapp.h \
util.h \
aboutdialog.h \
editablelabel.h
RESOURCES += mhcp.qrc

