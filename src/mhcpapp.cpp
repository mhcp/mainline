#include "mhcpapp.h"

#include <QDateTime>
#include <QtDebug>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QCoreApplication>
#include <QAction>
#include <QDateTime>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QTimer>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>
#include "historydialog.h"
#include "aboutdialog.h"
#include "util.h"
#include "platformutil.h"

namespace mhcp {

MhcpApp::MhcpApp(QObject* parent)
    : QObject(parent), notifications(true), historyDialog(NULL), aboutDialog(NULL)
{
    init();
    initGui();
}

MhcpApp::~MhcpApp()
{
    delete trayIcon;
    delete mhcpConfig;
    delete aboutDialog;
}

void MhcpApp::init()
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("Matties Healthcare program");

    connect(QCoreApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(saveStopTime()));

    mhcpConfig = new MhcpConfigDialog(this);

    if (!openOrCreateDB()) {
        qDebug() << "failed opening db.. quiting";
    }

    initDrinkTimer();
    leaveTimer.setSingleShot(true);
    connect(&leaveTimer, SIGNAL(timeout()), this, SLOT(onLeaveNotification()));
    initLeaveTimer();

    QTimer* pingTimer = new QTimer(this);
    lastPing = QDateTime::currentDateTime();
    connect(pingTimer, SIGNAL(timeout()), this, SLOT(onPing()));
    pingTimer->start(mhcpConfig->pingInterval);
}

void MhcpApp::initDrinkTimer()
{
    QTimer::singleShot(-mhcpConfig->drinkBreakFreq.msecsTo(QTime()), this, SLOT(onDrinkNotification()));
}

void MhcpApp::initLeaveTimer()
{
    startTime = getLastStartTime();
    //startTime.start();
    QTime endTime = addTime(startTime.time(), mhcpConfig->workDuration);
    qDebug() << "end time = " << endTime;

    leaveTimer.setInterval(QTime::currentTime().msecsTo(endTime));
    leaveTimer.start(); //restart
}

void MhcpApp::initGui()
{
    trayIcon = new QSystemTrayIcon(QIcon(":/images/bell.png"));

    qDebug() << "start time =" << startTime.toString();
    trayIcon->setToolTip(tr("Start time = %1").arg(startTime.toString("h:mm")));

    QMenu* contextMenu = new QMenu();

    enabledAction = new QAction("Enabled", contextMenu);
    enabledAction->setCheckable(true);
    enabledAction->setChecked(true);
    connect(enabledAction, SIGNAL(toggled(bool)), this, SLOT(onEnabledAction_toggled(bool)));
    contextMenu->addAction(enabledAction);

    QAction* muteAction = new QAction("Mute", contextMenu);
    muteAction->setCheckable(true);
    connect(muteAction, SIGNAL(toggled(bool)), this, SLOT(setMuteNotificationsEnabled(bool)));
    contextMenu->addAction(muteAction);

    contextMenu->addAction("Config", this, SLOT(onShowConfig()));

    contextMenu->addAction("Show history", this, SLOT(onShowHistory()));

    contextMenu->addSeparator();

    contextMenu->addAction("About", this, SLOT(onShowAboutDialog()));

    QAction* quitAction = new QAction("Quit", contextMenu);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(onQuit()));
    contextMenu->addAction(quitAction);

    trayIcon->setContextMenu(contextMenu);

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onTrayIconActivated(QSystemTrayIcon::ActivationReason)));
    trayIcon->show();


}

bool MhcpApp::isEnabled()
{
    return enabledAction->isChecked();
}

void MhcpApp::onEnabledAction_toggled(bool checked)
{
    trayIcon->setIcon(checked ? QIcon(":/images/bell.png") : QIcon(":/images/bell_disabled.png"));
}

//todo: let this method return QTime instead of QDateTime
QDateTime MhcpApp::getLastStartTime()
{
    QDateTime now = QDateTime::currentDateTime();
    QTime startTime = getTimeFromDB(now.date(), START_TIME);
    if (startTime.isNull()) {
        int millisBoot = PlatformUtil::getMilliSecsSinceBoot();
        if (mhcpConfig->useBootTimeThreshold > (millisBoot / 1000)) {
            qDebug() << "using boottime" << -millisBoot;
            now = now.addMSecs(-millisBoot);
        }
        insertTimeIntoDB(now, START_TIME);
        startTime = now.time();
    } else {
        clearStopTime(now.date());
    }
    now.setTime(startTime);
    return now;
}

bool MhcpApp::clearStopTime(QDate date)
{
    QString sql;
    sql = QString("UPDATE mhcp SET stopTime = null where date = '%1'").arg(date.toString(Qt::ISODate));
    QSqlQuery query(sql);
    if (!query.isActive()) qDebug() << "insert failed with error: " << query.lastError();
    return query.isActive();
}

bool MhcpApp::insertTimeIntoDB(QDateTime datetime, TIME_TYPE type, bool forceUpdate)
{
    QString sql;
    switch(type) {
    case START_TIME:
        if (!forceUpdate) {
            sql = QString("INSERT INTO mhcp VALUES(NULL,'%1', '%2', NULL)").arg(datetime.date().toString(Qt::ISODate)).arg(datetime.time().toString(Qt::ISODate));
        } else {
            sql = QString("UPDATE mhcp SET startTime = '%1' where date = '%2'").arg(datetime.time().toString(Qt::ISODate)).arg(datetime.date().toString(Qt::ISODate));
        }
        break;
            case STOP_TIME:
        sql = QString("UPDATE mhcp SET stopTime = '%1' where date = '%2'").arg(datetime.time().toString()).arg(datetime.date().toString(Qt::ISODate));
        break;
    }
    QSqlQuery query(sql);
    if (!query.isActive()) qDebug() << "insert failed with error: " << query.lastError();
    return query.isActive();
    //qDebug() << "determining last insert id";
    //rowId = query.lastInsertId().toInt();
}

bool MhcpApp::openOrCreateDB()
{
    //qDebug() << "SQLite driver available =" << QSqlDatabase::isDriverAvailable("QSQLITE");
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //qDebug() << "driver caps: querysize = " << db.driver()->hasFeature(QSqlDriver::QuerySize) << "lastinsertid = " << db.driver()->hasFeature(QSqlDriver::LastInsertId);
    qDebug() << "used db = " << mhcpConfig->dbPath;
    db.setDatabaseName(mhcpConfig->dbPath);
    if (!db.open()) {
        qDebug() << "failed opening db (TODO handle)";
        return false;
    }
    if (!db.tables().contains("mhcp")) {
        QSqlQuery query("CREATE TABLE mhcp (id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT NOT NULL, startTime TEXT NOT NULL, stopTime TEXT)");
        //if ( query.lastError().type() != QSqlError::NoError)
        if ( !query.isActive()) {
            return false;
        } else {
            qDebug() << "db successfully initialized";
        }
    }

    return true;
}

void MhcpApp::closeDB()
{
    QSqlDatabase::database().close();
}

QTime MhcpApp::getTimeFromDB(QDate date, TIME_TYPE type)
{
    //if (!openOrCreateDB()) {
    //	return QTime();
    //}

    QSqlQuery query("SELECT id, date, startTime, stopTime FROM mhcp WHERE date = '" + date.toString(Qt::ISODate) + "'");
    //qDebug() << "querystr" << query.executedQuery();
    //qDebug() << "query success = " << query.isActive() << "resultset size = " << query.size();
    if (query.isActive() && query.next()) {
        //rowId = query.value(0).toInt();
        QDate date = query.value(1).toDate();
        QTime startTime = query.value(2).toTime();
        QTime stopTime = query.value(3).toTime();
        //qDebug() << "previous stop time was " << stopTime.toString();
        //qDebug() << rowId << date << startTime;
        //sqlite does not support QSqlDriver::QuerySize
        if (query.next()) {
            qDebug() << "warning: db inconsistency!";
        }

        switch (type) {
        case START_TIME: return startTime;
        case STOP_TIME: return stopTime;
        }
    }
    return QTime();
    //db.close();
    //return startTime;
}

void MhcpApp::onShowAboutDialog()
{
    if (aboutDialog == NULL) {
        aboutDialog = new AboutDialog();
    }
    aboutDialog->show();
}

void MhcpApp::onShowConfig()
{
    mhcpConfig->exec();
}

void MhcpApp::onShowHistory()
{
    if (historyDialog == NULL) {
        historyDialog = new HistoryDialog(this);
    }
    historyDialog->show();
}

void MhcpApp::onQuit()
{
    qDebug() << "onQuit";
    //qDebug() << "app = " << app;
    QCoreApplication::quit();
}

void MhcpApp::onTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    qDebug() << "activation, reason: " << reason;
    switch(reason) {
    case QSystemTrayIcon::DoubleClick:
        qDebug() << "show dialog";
        break;
    case QSystemTrayIcon::Trigger:
        //QDateTime endTime = startDateTime.addSecs(mhcpConfig->workDuration.);
        trayIcon->showMessage("Status", tr("Start time = %1\nTime left = %2").arg(startTime.time().toString("h:mm")).arg(substractTime(mhcpConfig->workDuration,qtimeFromMSecs(startTime.time().elapsed())).toString("h:mm")));
        break;
    }
}

void MhcpApp::setMuteNotificationsEnabled(bool mute)
{
    notifications = !mute;
    trayIcon->setIcon(notifications ? QIcon(":/images/bell.png") : QIcon(":/images/bell_muted.png"));
}

void MhcpApp::onDrinkNotification()
{
    QString timeToDrink = tr("It's time to drink!");
    if (mhcpConfig->drinkNotificationType == "Balloon") {
        notifyBalloon(timeToDrink);
    } else if (mhcpConfig->drinkNotificationType == "Popup") {
        notifyPopup(timeToDrink);
    } else {
        qDebug() << "unknown notification type";
    }
    initDrinkTimer();
}

void MhcpApp::onLeaveNotification()
{
    QString timeToLeave = tr("It's time to leave!");
    if (mhcpConfig->drinkNotificationType == "Balloon") {
        notifyBalloon(timeToLeave);
    } else if (mhcpConfig->drinkNotificationType == "Popup") {
        notifyPopup(timeToLeave);
    } else {
        qDebug() << "unknown notification type";
    }
}


void MhcpApp::onPing()
{
    qDebug() << "onPing";
    if (!isEnabled()) return;
    QDateTime now = QDateTime::currentDateTime();
    bool dayShift = now.date().dayOfYear() != lastPing.date().dayOfYear();
    //are we still working..? ask user confirmation
    //show prompt unless there is a day shift and the config dictates to mute on that condition
    if (lastPing.secsTo(now) > mhcpConfig->workHiatusPromptSecs) {
        if (dayShift) {
            insertTimeIntoDB(lastPing, STOP_TIME);
            insertTimeIntoDB(now, START_TIME);
            trayIcon->showMessage("Good morning", QString("day switch\nlast activity: %1").arg(lastPing.toString(Qt::DefaultLocaleShortDate)));
            initLeaveTimer();
        } else if (mhcpConfig->promptUnlessDayShift) {
            enabledAction->setChecked(false);
            QMessageBox msgbox(QMessageBox::Question, "Wake up", "Still working..?", QMessageBox::Yes | QMessageBox::No);
            enabledAction->setChecked(msgbox.exec());
        }
    } else if (dayShift) {
        insertTimeIntoDB(lastPing, STOP_TIME);
        trayIcon->showMessage("Signing off for today", "MHCP disabled");
        enabledAction->setChecked(false);
        return;
    }
    lastPing = now;
}

void MhcpApp::notifyBalloon(QString text)
{
    trayIcon->showMessage("Ding dong", text);
}

void MhcpApp::notifyPopup(QString text)
{
    QMessageBox::critical(NULL, "MHCP Alert", text);
}

void MhcpApp::saveStopTime()
{
    qDebug() << "about to quit => try and save the stop time";
    QSqlDatabase db = QSqlDatabase::database();
    if (!db.isOpen()) {
        qDebug() << "reopen db";
        if (!db.open()) {
            qDebug() << "failed opening db when saving stop time";
            return;
        }
    }

    insertTimeIntoDB(QDateTime::currentDateTime(), STOP_TIME);
}

} //namespace mhcp
