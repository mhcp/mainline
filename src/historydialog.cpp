#include "historydialog.h"

#include <QtDebug>
#include <QCalendarWidget>
#include <QMouseEvent>

#include "util.h"
#include "ui_editdialog.h"

namespace mhcp {

HistoryDialog::HistoryDialog(MhcpApp* app)
: app(app)
{
	qDebug() << "creating history dialog";
	setupUi(this);
	initGUI();
}

void HistoryDialog::initGUI()
{
	calendarWidget->setFirstDayOfWeek(Qt::Monday);
	connect(calendarWidget, SIGNAL(selectionChanged()), this, SLOT(onSelectionChanged()));
        connect(calendarWidget, SIGNAL(activated(QDate)), this, SLOT(onEdit(QDate)));
	//connect(startLabel, SIGNAL(doubleClicked(QPoint& pos)), this, SLOT(onDoubleClick
	updateLabels(calendarWidget->selectedDate());
}

void HistoryDialog::showEvent ( QShowEvent * event )
{
	QDialog::showEvent(event);
	qDebug() << "showEvent";
	calendarWidget->setSelectedDate(QDate::currentDate());
}

void HistoryDialog::onEdit(QDate date)
{
    QDialog editDialog(this);
    Ui::EditDialog ui;
    ui.setupUi(&editDialog);
    QTime startTime = app->getTimeFromDB(date, START_TIME);
    QTime stopTime = app->getTimeFromDB(date, STOP_TIME);
    ui.startTE->setTime(startTime);
    ui.stopTE->setTime(stopTime);
    if (editDialog.exec() == QDialog::Accepted) {
        startTime = ui.startTE->time();
        stopTime = ui.stopTE->time();
        qDebug() << "updating" << date.toString() << "start = " << startTime << " stop = " << stopTime;
        app->insertTimeIntoDB(QDateTime(date,startTime), START_TIME, true);
        if (stopTime != QTime(0,0,0,0)) {
            app->insertTimeIntoDB(QDateTime(date,stopTime), STOP_TIME, true);
        }
        updateLabels(date);
        app->initLeaveTimer();
    }
}

void HistoryDialog::updateLabels(QDate date)
{
	QTime startTime = app->getTimeFromDB(date, START_TIME);
	QTime stopTime = app->getTimeFromDB(date, STOP_TIME);
//        qDebug() << "starttime= " << startTime;
//        qDebug() << "stoptime= " << stopTime;
	if (!startTime.isNull()) {
		startLabel->setText(startTime.toString("h:mm"));
	} else {
		startLabel->setText("<n/a>");
	}
	if (!stopTime.isNull()) {
		stopLabel->setText(stopTime.toString("h:mm"));
	} else {
		stopLabel->setText("<n/a>");
	}
	if (startTime.isNull() || stopTime.isNull()) {
		durationLabel->setText("<n/a>");
	} else {
		durationLabel->setText(substractTime(stopTime, startTime).toString("h:mm"));
	}

}

void HistoryDialog::onSelectionChanged()
{
	qDebug() << "selected date = " << calendarWidget->selectedDate();
	updateLabels(calendarWidget->selectedDate());
}


} //namespace mhcp
