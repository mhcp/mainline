#ifndef __ABOUTDIALOG_H__
#define __ABOUTDIALOG_H__

#include <QDialog>
#include <QObject>
#include <QtDebug>
#include <QString>

#include "ui_aboutdialog.h"

namespace mhcp {

class AboutDialog : public QDialog, private Ui::AboutDialog
{
	Q_OBJECT

public:
	AboutDialog();
	virtual ~AboutDialog() {};

	void initGUI();

};

} //namespace mhcp

#endif
