QT += sql widgets
HEADERS += src/util.h \
    src/mhcpapp.h \
    src/historydialog.h \
    src/hcpconfig.h \
    src/aboutdialog.h \
    src/platformutil.h
SOURCES += src/util.cpp \
    src/mhcpapp.cpp \
    src/main.cpp \
    src/historydialog.cpp \
    src/hcpconfig.cpp \
    src/aboutdialog.cpp \
    src/platformutil.cpp \
    src/platformutil_lin.cpp
win32 {
    SOURCES += src/platformutil_win.cpp
} else:unix {
}
FORMS += src/configdialog.ui \
    src/aboutdialog.ui \
    src/historydialog.ui \
    src/editdialog.ui
RESOURCES += src/mhcp.qrc
